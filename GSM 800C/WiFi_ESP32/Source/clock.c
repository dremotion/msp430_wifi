/*
 * clock.c
 *
 *  Created on: 23-Apr-2018
 *      Author: thispc
 */

#include <msp430.h>
#include <driverlib.h>

#define EXAMPLE_CODE            1

#define LF_CRYSTAL_FREQUENCY_IN_HZ                      32768
#define HF_CRYSTAL_FREQUENCY_IN_HZ                      8000000



void Init_Smclk_8Mhz(void)
{
#ifdef EXAMPLE_CODE

    // Clock System Setup
    CSCTL0_H = CSKEY_H;                     // Unlock CS registers
    CSCTL1 = DCOFSEL_0;                     // Set DCO to 1MHz
    // Set SMCLK = MCLK = DCO, ACLK = VLOCLK
    CSCTL2 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK;

    // Per Device Errata set divider to 4 before changing frequency to
    // prevent out of spec operation from overshoot transient
    CSCTL3 = DIVA__4 | DIVS__4 | DIVM__4;   // Set all corresponding clk sources to divide by 4 for errata
    CSCTL1 = DCOFSEL_6;                     // Set DCO to 8MHz

    // Delay by ~10us to let DCO settle. 60 cycles = 20 cycles buffer + (10us / (1/4MHz))
    __delay_cycles(60);
    CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;   // Set all dividers to 1 for 8MHz operation
    CSCTL0_H = 0;


#else
    CSCTL0_H    = CSKEY_H;
    CSCTL1      = 0x48;
    CSCTL2      = SELA_1 | SELS_3 | SELM_3;//VLOCK | DCOCLK | MODCLK
    CSCTL3      = 0x0000;
    __delay_cycles(160);//ToDo: Add delay of ~10-12usec
    CSCTL0_H    = 0x0000;

#endif
    return;
}


/*
 * Purpose  : Check Clock values
 */
void Check_Clock(void)
{
   unsigned long int aclk = 0, smclk = 0, mclk = 0;

   aclk     = CS_getACLK();
   smclk    = CS_getSMCLK();
   mclk     = CS_getMCLK();
return;
}






void Init_Clock(void)
{
#ifdef LOW_FREQ
    /*
     * Set LFXT clock pins to crystal input. By default, they're configured as GPIO.
     * If you don't configure these pins, your code will be stuck in CS_turnOnLFXT function
     */
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_PJ, GPIO_PIN4 + GPIO_PIN5, GPIO_PRIMARY_MODULE_FUNCTION);
    //CS_setExternalClockSource( 32768, 0 );

    /*
     * Initialize LFXT crystal oscillator without timeout. In case of crystal failure
     * the code remains 'stuck' in this function.
     */
    CS_turnOnLFXT( CS_LFXT_DRIVE_0 );

    // Set DCO to run at 8MHz
    CS_setDCOFreq( CS_DCORSEL_1, CS_DCOFSEL_3 );

    // Set ACLK to use LFXT as its oscillator source (32KHz)
    CS_initClockSignal( CS_ACLK, CS_LFXTCLK_SELECT, CS_CLOCK_DIVIDER_1 );
    // Set ACLK to use VLOCLK as its oscillator source (10KHz)
    //CS_initClockSignal( CS_ACLK, CS_VLOCLK_SELECT, CS_CLOCK_DIVIDER_1 );

    // Set SMCLK to use DCO as its oscillator source
    CS_initClockSignal( CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1 );

    // Set MCLK to use DCO as its oscillator source
    CS_initClockSignal( CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1 );
#else
    //FRAMCtl_configureWaitStateControl(FRAMCTL_ACCESS_TIME_CYCLES_7);

    //FRAMCtl_configureWaitStateControl(FRAMCTL_ACCESS_TIME_CYCLES_7);
    /*
     * Set LFXT and HFXT clock pins to crystal input. By default, they're configured as GPIO.
     * If you don't configure these pins, your code will be "stuck" in CS_turnOnLFXT function
     */
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_PJ, GPIO_PIN4 + GPIO_PIN5 + GPIO_PIN6 + GPIO_PIN7, GPIO_PRIMARY_MODULE_FUNCTION);

    CS_setExternalClockSource(LF_CRYSTAL_FREQUENCY_IN_HZ, HF_CRYSTAL_FREQUENCY_IN_HZ);
    /*
     * Initialize LFXT crystal oscillator without timeout. In case of failure
     * the code remains 'stuck' in this function.
     */
    CS_turnOnLFXT(CS_LFXT_DRIVE_0);
    /*
     * Initialize HFXT crystal oscillator without timeout. In case of failure
     * the code remains 'stuck' in this function.
     */
    CS_turnOnHFXT(CS_HFXT_DRIVE_8MHZ_16MHZ);
    //Set ACLK to use LFXT as its oscillator source (32KHz)
    CS_initClockSignal(CS_ACLK, CS_LFXTCLK_SELECT, CS_CLOCK_DIVIDER_1);
    //Set SMCLK to use HFXT as its oscillator source (16MHz) SMCLK = 8 MHz
    CS_initClockSignal(CS_SMCLK, CS_HFXTCLK_SELECT, CS_CLOCK_DIVIDER_1);
    //Set MCLK to use HFXT as its oscillator source (16MHz)
    CS_initClockSignal(CS_MCLK, CS_HFXTCLK_SELECT, CS_CLOCK_DIVIDER_1);

#endif
}


