/*
 * macros.h
 *
 *  Created on: 28-Apr-2018
 *      Author: thispc
 */

#ifndef MACROS_H_
#define MACROS_H_


#define WDT_STOP          WDTCTL = WDTPW | WDTHOLD
#define GPIO_UNLOCK       PM5CTL0 &= ~LOCKLPM5
#define ENABLE_GIE        __bis_SR_register(GIE)

#define FUNC_SUCCESS      0
#define FUNC_FAIL         1
#define CLEAR             0
#define SET               1


/******** Buffer Sizes *********/
#define RX_GSM_BUFF_SIZE        200

#endif /* MACROS_H_ */
