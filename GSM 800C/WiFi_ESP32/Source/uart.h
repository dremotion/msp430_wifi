/*
 * uart.h
 *
 *  Created on: 28-Apr-2018
 *      Author: thispc
 */

#ifndef SOURCE_UART_H_
#define SOURCE_UART_H_

void Init_Uart0(void);
void Init_Uart1(void);
void Init_Uart3(void);

void UART0_Transmit(unsigned char* cmd, unsigned int len)  ;
void UART1_Transmit(unsigned char* cmd, unsigned int len)  ;
void UART3_Transmit(unsigned char* cmd, unsigned int len)  ;     // send to UART_A1_TX.;

#endif /* SOURCE_UART_H_ */
