/*
 * board.c
 *
 *  Created on: 28-Apr-2018
 *      Author: thispc
 */


#include <msp430.h>
#include <driverlib.h>
#include "macros.h"
#include "clock.h"
#include "uart.h"


void Init_Board(void)
{
    WDT_STOP;
    GPIO_UNLOCK;
    ENABLE_GIE;
    Init_Smclk_8Mhz();
#ifdef DEBUG_MODE
    Check_Clock();
#endif
//PWRKey Pin
//Status Pin
    Init_Uart0();
    Init_Uart1();
//On Board Uart - GSM/WiFi
    Init_Uart3();




}
