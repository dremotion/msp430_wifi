/*  Date            :   28th April, 2018
 *  Title           :   WiFi_ESP32
 *  Documentation   :   https://www.espressif.com/sites/default/files/documentation/esp32_at_instruction_set_and_examples_en.pdf
 *  AT Commands     :   https://github.com/espressif/esp32-at
 */

#include "main.h"


unsigned char received_buffer[RX_GSM_BUFF_SIZE] = {0};
unsigned int rx_buffer_index = 0;

void main(void)
{
    Init_Board();


    while(1)
    {
        UART3_Transmit("Hello", 5);
    }
}
