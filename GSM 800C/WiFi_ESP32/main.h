/*
 * main.h
 *
 *  Created on: 28-Apr-2018
 *      Author: thispc
 */

#ifndef MAIN_H_
#define MAIN_H_


#include <msp430.h>
#include <driverlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "MQTTPacket.h"

#include "Source/macros.h"
#include "Source/board.h"
#include "Source/uart.h"




extern unsigned char received_buffer[RX_GSM_BUFF_SIZE];
extern unsigned int rx_buffer_index;



#endif /* MAIN_H_ */
