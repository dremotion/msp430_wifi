/*
 * uart.c
 *
 *  Created on: 28-Apr-2018
 *      Author: thispc
 */


#include <msp430.h>
#include <driverlib.h>
#include "macros.h"


/*
 * Function : Init_Uart0()
 * Purpose  : Configure Uart0 @9600
 * Status   : Tested | Passed
 */
void Init_Uart0(void)
{
    /****  UART A0 Pins *****/
    P2DIR     |=  0x01;                 //P2.0 and P2.1
    P2SEL1    |=  0x03;
    P2SEL0    &= ~0x03;

    /***** Uart0 Registers ****/
    UCA0CTLW0  = UCSSEL_2 | UCSWRST_1;    //Parity_Disbale | 8bit_data | UART_Asynchronous_Mode | SMCLK | Software_Reset_Enable (UCSWRST = 1)
    UCA0CTLW1 |= 0x0000;                  //Register for deglitch time --> Not messing with this right now | Calculating needs integration

    UCA0BRW_H    = 0x00;
    UCA0BRW_L    =   52;                  //16-bit Integer divisors are possible because there are two registers, UBR00 and UBR01, both 8-bit.
    //UBR00 contains the upper byte while UBR00 contains the lower byte (lower 8 bits). In this case
    //3 is not greater than 255 so that we place 0x00 in the upper byte and 3 (or its hex equivalent 0x03)
    //on the lower byte. Recall the pdf saved in your Google_Drive

    UCA0MCTLW  = 0x4911;                  // 49 is for UCBRSx -- 1 is for UCBRFx -- 1 is for UCOS16 (we are in over_sampling)

    UCA0STATW |= 0x0000;                  // Contains about error flags and status of UART - IG
    //Ignoring registers: Tx_Rx_Buffers , Auto_Baud_Ctrl, Encoding_Decoding_reg

    UCA0CTLW0 &= ~0x0001;                 //Once all the settings are complete, we can enable the module --> Initialize UART state machine
    //UCA0IE    |=  0x0001;               //Enable Transmitter and Receiver Interrupts

}


/*
 * Function : Init_Uart1()
 * Purpose  : Configure Uart1 @9600
 * Status   : Untested
 */
void Init_Uart1(void)
{
    /****  UART A1 Pins *****/
    P2DIR  |=  0x20;                    //P2.5->Tx and P2.6->Rx
    P2SEL1 |=  0x60;
    P2SEL0 &= ~0x00;

    /***** Uart1 Registers ****/
    UCA1CTLW0  = 0x0081;                  //Parity_Disbale | 8bit_data | UART_Asynchronous_Mode | SMCLK | Software_Reset_Enable (UCSWRST = 1)
    UCA1CTLW1 |= 0x0000;                  //Register for deglitch time --> Not messing with this right now | Calculating needs integration

    UCA1BRW_H    = 0x00;
    UCA1BRW_L    =   52;                  //16-bit Integer divisors are possible because there are two registers, UBR00 and UBR01, both 8-bit.
    //UBR00 contains the upper byte while UBR00 contains the lower byte (lower 8 bits). In this case
    //3 is not greater than 255 so that we place 0x00 in the upper byte and 3 (or its hex equivalent 0x03)
    //on the lower byte. Recall the pdf saved in your Google_Drive

    UCA1MCTLW  = 0x4911;                  // 49 is for UCBRSx -- 1 is for UCBRFx -- 1 is for UCOS16 (we are in over_sampling)

    UCA1STATW |= 0x0000;                  // Contains about error flags and status of UART - IG
    //Ignoring registers: Tx_Rx_Buffers , Auto_Baud_Ctrl, Encoding_Decoding_reg

    UCA1CTLW0 &= ~0x0001;                 //Once all the settings are complete, we can enable the module --> Initialize UART state machine
    //UCA1IE    |=  0x0001;               //Enable Transmitter and Receiver Interrupts

}



/*
 * Function : Init_Uart3()
 * Purpose  : Configure Uart3 @9600
 * Status   : Untested
 */
void Init_Uart3(void)
{
    /****  UART A1 Pins *****/
    P6DIR  |=  BIT0;                    //P2.5->Tx and P2.6->Rx
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P6, GPIO_PIN0+GPIO_PIN1, GPIO_PRIMARY_MODULE_FUNCTION);

    /***** Uart1 Registers ****/
    UCA1CTLW0  = 0x0081;                  //Parity_Disbale | 8bit_data | UART_Asynchronous_Mode | SMCLK | Software_Reset_Enable (UCSWRST = 1)
    UCA1CTLW1 |= 0x0000;                  //Register for deglitch time --> Not messing with this right now | Calculating needs integration

    UCA1BRW_H    = 0x00;
    UCA1BRW_L    =   52;                  //16-bit Integer divisors are possible because there are two registers, UBR00 and UBR01, both 8-bit.
    //UBR00 contains the upper byte while UBR00 contains the lower byte (lower 8 bits). In this case
    //3 is not greater than 255 so that we place 0x00 in the upper byte and 3 (or its hex equivalent 0x03)
    //on the lower byte. Recall the pdf saved in your Google_Drive

    UCA1MCTLW  = 0x4911;                  // 49 is for UCBRSx -- 1 is for UCBRFx -- 1 is for UCOS16 (we are in over_sampling)

    UCA1STATW |= 0x0000;                  // Contains about error flags and status of UART - IG
    //Ignoring registers: Tx_Rx_Buffers , Auto_Baud_Ctrl, Encoding_Decoding_reg

    UCA1CTLW0 &= ~0x0001;                 //Once all the settings are complete, we can enable the module --> Initialize UART state machine
    //UCA1IE    |=  0x0001;               //Enable Transmitter and Receiver Interrupts

}




/************************** Uart Transmit ******************************/
void UART0_Transmit(unsigned char* cmd, unsigned int len)        // send to UART_A1_TX.
{
    while (len--)
    {
        while(!(UCA0IFG & UCTXIFG));
        UCA0TXBUF = *cmd++;
    }
}


void UART1_Transmit(unsigned char* cmd, unsigned int len)        // send to UART_A1_TX.
{
    while (len--)
    {
        while(!(UCA1IFG & UCTXIFG));
        UCA1TXBUF = *cmd++;
    }
}


void UART1_Transmit(unsigned char* cmd, unsigned int len)        // send to UART_A1_TX.
{
    while (len--)
    {
        while(!(UCA3IFG & UCTXIFG));
        UCA3TXBUF = *cmd++;
    }
}

